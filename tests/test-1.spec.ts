import { test, expect } from '@playwright/test';

test('test', async ({ page }) => {
  page.setViewportSize({ width: 1440, height: 900 });
  await page.goto('http://103.80.48.96:23000/user/signin');
  await page.locator('#signin-email').click();
  await page.locator('#signin-email').fill('pornsawank.work@gmail.com');
  await page.locator('#signin-password').click();
  await page.locator('#signin-password').fill('12345678');
  await page.getByRole('img', { name: 'icon hide password' }).click();
  await page.getByRole('button', { name: 'เข้าใช้ระบบ' }).click();
  await page.getByRole('link', { name: 'logo-add-business เพิ่มธุรกิจ' }).click();
  await page.locator('.container-fluid').click();
  await page.locator('#businessDescription').press('Meta+-');
  await page.locator('#businessDescription').press('Meta+-');
  await page.locator('#businessDescription').press('Meta+-');
  await page.locator('#businessDescription').press('Meta+=');
  await page.locator('#businessDescription').press('Meta+=');
  await page.locator('div').filter({ hasText: 'สร้างธุรกิจใหม่ข้อมูลองค์กรสร้างธุรกิจใหม่บัญชีของฉันเปลี่ยนรหัสผ่านออกจากระบบ' }).nth(2).click();
  await page.locator('#businessNameTh').click();
  await page.locator('#businessNameTh').fill('Gattino Cattery-บ้านแมวสก็อตติชโฟลด์');
  await page.locator('#businessDescription').click();
  await page.locator('#businessDescription').fill('Gattino Cattery-บ้านแมวสก็อตติชโฟลด์');
  await page.locator('#businessDescription').click();
  await page.getByText('Gattino Cattery-บ้านแมวสก็อตติชโฟลด์').fill('Gattino Cattery-บ้านแมวสก็อตติชโฟลด์');
  await page.getByText('Gattino Cattery-บ้านแมวสก็อตติชโฟลด์').press('Enter');
  await page.getByText('Gattino Cattery-บ้านแมวสก็อตติชโฟลด์').fill('Gattino Cattery-บ้านแมวสก็อตติชโฟลด์\nบ้านแมวสก็อตติชโฟลด์เพื่อคนรักแมว');
  await page.getByRole('combobox').first().selectOption('1');
  await page.getByRole('combobox').nth(1).selectOption('C');
  await page.locator('input[type="date"]').fill('2023-03-02');
  await page.locator('.col-md-8').click();
  await page.locator('#businessRegisterCost').click();
  await page.locator('#businessRegisterCost').fill('200000');
  await page.locator('#businessNumber').click();
  await page.locator('#businessNumber').fill('19200202');
  await page.locator('div').filter({ hasText: /^ที่อยู่ถนนรหัสไปรษณีย์$/ }).locator('#address').click();
  await page.locator('div').filter({ hasText: /^ที่อยู่ถนนรหัสไปรษณีย์$/ }).locator('#address').fill('90/890');
  await page.locator('#road').click();
  await page.getByPlaceholder('กรุณากรอกรหัสไปรษณีย์').click();
  await page.getByPlaceholder('กรุณากรอกรหัสไปรษณีย์').fill('11110');
  await page.getByRole('combobox').nth(2).selectOption('3');
  await page.getByRole('combobox').nth(3).selectOption('61');
  await page.getByRole('combobox').nth(4).selectOption('120408');
  await page.locator('div').filter({ hasText: /^FacebookInstagramYoutube$/ }).locator('#address').click();
  await page.locator('div').filter({ hasText: /^FacebookInstagramYoutube$/ }).locator('#address').click();
  await page.locator('div').filter({ hasText: /^FacebookInstagramYoutube$/ }).locator('#address').fill('https://www.facebook.com/profile.php?id=100083317360034');
  await page.getByRole('button', { name: 'บันทึก' }).click();
});